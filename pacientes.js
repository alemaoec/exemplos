var inputNome = document.querySelector('#nome')
var inputPeso = document.querySelector('#peso')
var inputAltura = document.querySelector('#altura')
var inputGordura = document.querySelector('#gordura')

var botaoAdicionar = document.querySelector('#adicionar-paciente')
botaoAdicionar.onclick = function(evento) {
	evento.preventDefault();
	var nome = inputNome.value;
	var peso = inputPeso.value;
	var altura = inputAltura.value;
	var gordura = inputGordura.value;	
	mostrarPaciente(nome, peso, altura, gordura)
}

var httpCliente = new XMLHttpRequest();
httpCliente.open('GET', 'https://api-pacientes.herokuapp.com/pacientes')
httpCliente.onreadystatechange = function() {
	if(httpCliente.readyState == 4){
		var resposta = JSON.parse(httpCliente.responseText);
		for(var i = 0; i < resposta.length; i ++){
				mostrarPaciente(resposta[i].nome, resposta[i].peso, resposta[i].altura, resposta[i].gordura);
		}
	}
}
httpCliente.send();
function mostrarPaciente(nome, peso, altura, gordura){
	var novoPaciente = "<tr class='paciente' >" +
              "<td class='info-nome'>"+nome+"</td>" +
              "<td class='info-peso'>"+peso+"</td>" +
              "<td class='info-altura'>"+altura+"</td>" +
              "<td class='info-gordura'>"+gordura+"</td>" +
              "<td class='info-imc'>0</td>" +
            "</tr>";
   
  var tabela = document.querySelector("#tabela-pacientes")
	var conteudoExistente = tabela.innerHTML
	conteudoExistente = conteudoExistente + novoPaciente
	tabela.innerHTML = conteudoExistente	
}